﻿using System;

namespace GitPullLab
{
    class Program
    {
        private static int guess;
        private static int number;

        static void Main(string[] args)
        {
            Random rnd = new Random();
            number = rnd.Next(1, 10);

            StartGame();
            CheckAns();
        }

        private static void StartGame()
        {
            Console.WriteLine("I have thought of a random number between 1 and 10. What do you think it is?");
            if (!Int32.TryParse(Console.ReadLine(), out guess))
            {
                Console.WriteLine("Not a number! Try again!");
                StartGame();
                Console.WriteLine("GameStart");
            }

            if (guess > 10 || guess < 1)
            {
                Console.WriteLine("Out of range! Try again!");
                StartGame();
                Console.WriteLine("GameStart");
            }
        }

        private static void CheckAns()
        {
            if (guess == number) Console.WriteLine("Your guess was correct!");
            else Console.WriteLine(string.Format("Your guess was wrong! The number was {0}", number.ToString()));
        }
    }
}
